const firstName = document.getElementById("firstName");
const lastName = document.getElementById("lastName");
const fullName = document.getElementById("fullName");
const color = document.getElementById("color");

const fullNameKo = () => {
    fullName.innerHTML = `${firstName.value} ${lastName.value}`;
};

firstName.addEventListener("keyup", fullNameKo);
lastName.addEventListener("keyup", fullNameKo);

color.addEventListener("change", (event) => {
    fullName.style.color = event.target.value;
});
